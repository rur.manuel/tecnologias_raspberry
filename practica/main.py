import websocket
try:
    import thread
except ImportError:
    import _thread as thread
from threading import Timer
import json

serverIp = "ws://192.168.1.71:8082"

def sendMessage(ws, message):
  ws.send(message)

def onMessage(ws, message):
  try:
    data = json.loads(message)
    if data["action"] == "register":
      print("Se ha registrado como:", data["id"])
  except json.JSONDecodeError:
    print("Error decoding JSON")
  except:
    print("Uknown error")

def onError(ws, error):
  print(error)

def onClose(ws):
  print("### closed ###")

def onOpen(ws):
  def run(*args):
    # Ejecuta la funcion despues de 5 segundos
    Timer(5, sendMessage, (ws, "Hello world"))
  thread.start_new_thread(run, ())


if __name__ == "__main__":
  websocket.enableTrace(True)
  ws = websocket.WebSocketApp(
    serverIp,
    on_message = onMessage,
    on_error = onError,
    on_close = onClose
  )
  ws.on_open = onOpen
  ws.run_forever()