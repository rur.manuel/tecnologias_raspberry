import RPi.GPIO as GPIO
import time

servoPIN = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

p = GPIO.PWM(servoPIN, 50) # GPIO pin 17 con PWM de 50Hz
p.start(2.5) # Inicializacion

maxPos = 180
minPos = 0

def translate(sensor_val, in_from, in_to, out_from, out_to):
    out_range = out_to - out_from
    in_range = in_to - in_from
    in_val = sensor_val - in_from
    val=(float(in_val)/in_range)*out_range
    out_val = out_from+val
    return out_val

def getPwm(angle):
  return translate(angle, minPos, maxPos, 2.5, 12.5)

def goToPos(pos, initialPos):
  step = 35 if pos > initialPos else -35
  i = 0
  for i in range(initialPos, pos, step):
    p.ChangeDutyCycle(getPwm(i))
    time.sleep(0.1)
  if i != pos:
    p.ChangeDutyCycle(getPwm(pos))

try:
  while True:
    goToPos(180, 0)
    time.sleep(3)
    goToPos(0, 180)
    time.sleep(3)
except KeyboardInterrupt:
  p.stop()
  GPIO.cleanup()
