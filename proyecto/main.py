import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time
import json
from motor import open as openDoor

serverIp = "ws://192.168.1.78:3000/raspberry"

initialCode = '1234'
initialId = 'door1'
deviceData = None

def onDeviceRegistered(data):
  deviceData = data["device"]
  print(deviceData)


def resolveEvent(action, data):
  if action == "register":
    onDeviceRegistered(data)
  elif action == "open":
    openDoor()
  else:
    print("Invalid action")

def onMessage(ws, message):
  try:
    data = json.loads(message)
    resolveEvent(data["action"], data)
  except json.JSONDecodeError:
    print("Error al procesar el mensaje")
  except:
    print("Error inesperado")

def onError(ws, error):
  print(error)

def onClose(ws):
  print("### closed ###")

def onOpen(ws):
  def run(*args):
    # Registra el dispositivo para que pueda comunicarse con el cliente
    requestBidy = json.dumps({
      "action": "register",
      "code": initialCode,
      "id": initialId
    })
    ws.send(requestBidy)
  thread.start_new_thread(run, ())


if __name__ == "__main__":
  websocket.enableTrace(True)
  ws = websocket.WebSocketApp(
    serverIp,
    on_message = onMessage,
    on_error = onError,
    on_close = onClose
  )
  ws.on_open = onOpen
  ws.run_forever()